# base image
FROM alpine:3.17
# add metadata
LABEL maintainer="somprasong@hospital-os.com"
# run unix command
RUN apk update && \
    apk --no-cache add ca-certificates nginx
# create system environment
ENV MY_TEST_SITE_DIR=/mytestsite
# change working directory
WORKDIR $MY_TEST_SITE_DIR
# copy from file
COPY ./index.html .
# copy from file or URL
ADD ./default.conf /etc/nginx/http.d/default.conf
# expose network port
EXPOSE 80
# command that run when container start
CMD  nginx -g "daemon off;"